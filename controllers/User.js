'use strict'

const User = require('../models/User');
const userservice = require('../services/userService');

function SignUp(req, res) {
    const user = new User({
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        rut: req.body.apellido,
        password: req.body.password,
        estado_financiero: 
        {
            codigo: req.body.estado_financiero.codigo,
            estado: req.body.estado_financiero.estado
        },
        cuentas_pago: [{
            nro_cuenta: req.body.cuentas_pago.nro_cuenta,
            banco: req.body.cuentas_pago.banco,
            nro_tarjeta: req.body.cuentas_pago.nro_tarjeta,
            fecha_vencimiento: req.body.cuentas_pago.fecha_vencimiento
        }]
    })
    user.save((err) => {
        if (err) res.status(500).send({ message: `error al registrar usuario` })
        return res.status(200).send({ token: userservice.createToken(user) })
    })
}

function SignIn(req, res) {
    User.findOne({ rut: req.body.rut, password: req.body.password }, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: `no existe el usuario` })
        req.user = user
        res.status(200).send({
            message: `te has logueado correctamente`,
            token: userservice.createToken(user)
        })
    })
}

function getUser(req, res) {
    let userId = req.params.userId
    User.findById(userId, (err, user) => {
        if (err) {
            return res.status(500).send({ message: 'Error al obtener los usuarios' })
        }
        else {
            res.status(200).send({ user })
        }
        if (!user) {
            return res.status(404).send({ message: 'No existen usuarios' })
        }
        else {
            res.status(200).send({ user })
        }
    })
}

function getUsers(req, res) {
    User.find({}, (err, user) => {
        if (err) { return res.status(500).send({ message: 'Error al obtener los usuarios' }) }
        if (!user) { return res.status(404).send({ message: 'No existen usuario' }) }
        res.status(200).send({ user })
    })
}

function updateUser(req, res) {
    let userId = req.params.userId
    let update = req.body

    User.findByIdAndUpdate(userId, update, (err, userUpdated) => {
        if (err) res.status(500).send({ message: `Error al actualizar el usuario ${err}` })

        res.status(200).send({ message: `Usuario Actualizado ${userUpdated}` })
    })
}

function deleteUser(req, res) {
    let userId = req.params.userId;
    User.findById(userId, (err, user) => {
        if (err) res.status(500).send({ message: `Error al borrar el usuario ${err}` })

        user.remove(err => {
            if (err) res.status(500).send({ message: `Error al borrar el usuario ${err}` })
            res.status(200).send({ message: `Usuario Borrado` })
        })
    })
}

function saveUser(req, res) {
    console.log(req.body)
    const user = new User({
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        rut: req.body.apellido,
        password: req.body.password,
        estado_financiero: 
        {
            codigo: req.body.estado_financiero.codigo,
            estado: req.body.estado_financiero.estado
        },
        cuentas_pago: [{
            nro_cuenta: req.body.cuentas_pago.nro_cuenta,
            banco: req.body.cuentas_pago.banco,
            nro_tarjeta: req.body.cuentas_pago.nro_tarjeta,
            fecha_vencimiento: req.body.cuentas_pago.fecha_vencimiento
        }]
    })
    user.save((err, userStored) => {
        if (err) {
            res.status(500).send({ message: 'Error al guardar' })
        }
        else {
            res.status(200).send({ userStored })
        }
    })
}

module.exports = {
    SignUp,
    SignIn,
    getUser,
    getUsers,
    updateUser,
    deleteUser,
    saveUser
}