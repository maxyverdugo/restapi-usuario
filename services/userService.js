'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config');

function createToken(user) {
    const payload = {
        sub: user.rut,
        iat: moment.unix(),
        exp: moment.duration(14, 'days')
    }
    return jwt.encode(payload, config.SECRET_TOKEN)
}

function decodeToken(token) {
    const decoded = new Promise((resolve, reject) => {
        try {
            const payload = jwt.decode(token, config.SECRET_TOKEN);
            if(payload.exp < moment.unix()){
                reject({
                    status: 401,
                    message: 'el token ha expirado'
                })
            }
            resolve(payload.sub)
        } catch(err) {
            reject({
                status: 500,
                message: 'invalid token'
            });
        }
    })
    return decoded;
}

module.exports = {
    createToken,
    decodeToken
}