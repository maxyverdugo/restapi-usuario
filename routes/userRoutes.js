'use strict'

const express = require('express');
const Usercontroller = require('../controllers/User')
const auth = require('../middlewares/auth');
const api = express.Router(); //Funcion Route de NodeJs *Genera un enrutador*

api.get('/users', Usercontroller.getUsers);
api.get('/user/:userId', Usercontroller.getUser); 
api.post('/user', Usercontroller.saveUser);
api.put('/user/:userId', Usercontroller.updateUser);
api.delete('/user/:userId', Usercontroller.deleteUser);
api.post('/signup', Usercontroller.SignUp);
api.post('/signin', Usercontroller.SignIn);
api.get('/private', auth, (req, res) => {
    res.status(200).send({message: `correcto`})
})
module.exports = api