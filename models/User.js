'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

const UserSchema = Schema({
    nombre: String,
    apellido: String,
    rut: String,
    password: String,
    fecha_nacimiento: String,
    estado_financiero: 
    {
            codigo: Number,
            estado: String
    },
    cuentas_pago: [
        {
            nro_cuenta: Number,
            banco: String,
            nro_tarjeta: Number,
            fecha_vencimiento: String
        }
    ]
})

UserSchema.pre('save', function (next) {
    let user = this
    if(!user.isModified('password')) return next()

    bcrypt.genSalt(10, (err, salt) => {
        if(err) return next()
        bcrypt.hash(user.password, salt, null, (err, hash) => {
            if(err) return next(err)

            user.password = hash;
            next()
        })
    })
})

module.exports = mongoose.model('users', UserSchema);